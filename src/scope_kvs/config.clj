(ns scope-kvs.config
  (:import [jetbrains.exodus.env StoreConfig
            Environment Transaction]))


(def ^:private default-config {:store "default"
                               :mode :without-duplicates-with-prefixing})

(defn- ^StoreConfig mode->store-config [mode]
  (case mode
    ;; Duplicates are not yet supported.
    ; :with-duplicates                   StoreConfig/WITH_DUPLICATES
    :without-duplicates                StoreConfig/WITHOUT_DUPLICATES
    ;:with-duplicates-with-prefixing    StoreConfig/WITH_DUPLICATES_WITH_PREFIXING
    :without-duplicates-with-prefixing StoreConfig/WITHOUT_DUPLICATES_WITH_PREFIXING
    (ex-info "Invalid or unsupported mode!" {:mode mode})))

(defn- config-with-defaults [config]
  (merge default-config config))

(defn config->store [config ^Environment env ^Transaction tx]
  (let [cfg (config-with-defaults config)]
    (.openStore env
                (:store cfg)
                (mode->store-config (:mode cfg))
                tx)))
