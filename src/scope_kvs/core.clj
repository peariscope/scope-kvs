(ns scope-kvs.core
  (:import [jetbrains.exodus.env
            Environment Environments
            Store Transaction TransactionalExecutable])
  (:require [scope-kvs.serializer :as ser]
            [scope-kvs.config :as cfg]))

;;
;; Transaction management
;;
(defn- in-env [cfg f]
  (let [^Environment env (Environments/newInstance (:path cfg))]
    (try (f env)
         (finally (.close env)))))

(defn- transactional-executable
  ([f]
   (proxy [TransactionalExecutable] []
     (execute [^Transaction txn]
       (f txn))))

  ([cfg env f]
   (transactional-executable
    (fn [tx]
      (let [^Store store (cfg/config->store cfg env tx)]
        (f {:env env :store store :tx tx}))))))

(defn in-transaction [cfg f]
  (in-env cfg (fn [^Environment env]
                (.executeInTransaction
                 env
                 (transactional-executable cfg env f)))))

(defn in-readonly-transaction [cfg f]
  (in-env cfg (fn [^Environment env]
                (.executeInReadonlyTransaction
                 env
                 (transactional-executable cfg env f)))))

(defn in-exclusive-transaction [cfg f]
  (in-env cfg (fn [^Environment env]
                (.executeInExclusiveTransaction
                 env
                 (transactional-executable cfg env f)))))

;;
;; Getting and setting values
;;
(defn- method-selector [cfg]
  (if (:tx cfg)
    :transaction
    :method))


(defmulti put! (fn [cfg k v] (method-selector cfg)))

(defmethod put! :transaction [tx-map k v]
  (let [{^Store store     :store
         ^Transaction tx  :tx} tx-map]
    (.put store tx (ser/clj->entry k) (ser/clj->entry v))))

(defmethod put! :method [cfg k v]
  (in-transaction cfg (fn [tx] (put! tx k v))))


(defmulti merge! (fn [cfg m] (method-selector cfg)))

(defmethod merge! :transaction [tx-map m]
  (let [{^Store store    :store
         ^Transaction tx :tx} tx-map
        kv-pairs              (into [] m)]
    (doseq [[k v] kv-pairs]
      (.put store tx (ser/clj->entry k) (ser/clj->entry v)))))

(defmethod merge! :method [cfg m]
  (in-transaction cfg (fn [tx] (merge! tx m))))


(defmulti get-value (fn [cfg k] (method-selector cfg)))

(defmethod get-value :transaction [tx-map k]
  (let [{^Store store :store
         ^Transaction tx :tx} tx-map]
    (ser/entry->clj (.get store tx (ser/clj->entry k)))))

(defmethod get-value :method [cfg k]
  (let [result (promise)]
    (try (in-readonly-transaction
          cfg
          (fn [tx] (deliver result (get-value tx k))))
         (catch Exception e
           (deliver result nil)
           (throw e)))
    @result))


(defmulti get-map (fn [cfg ks] (method-selector cfg)))

(defmethod get-map :transaction [tx-map ks]
  (let [{^Store store    :store
         ^Transaction tx :tx} tx-map
        pairs (for [k ks]
                [k (ser/entry->clj (.get store tx (ser/clj->entry k)))])
        value (into {} pairs)]
    value))

(defmethod get-map :method [cfg ks]
  (let [result (promise)]
    (try (in-readonly-transaction
          cfg
          (fn [tx] (deliver result (get-map tx ks))))
         (catch Exception e
           (deliver result nil)
           (throw e)))
    @result))

(comment
  ;; A live REPL demo! Execute away!
  (def cfg {:path "/tmp/scope_kvs_demo"})

  ;; Put/get values from the store.
  (put! cfg :number 7)
  (get-value cfg :number)

  ;; Put/get multiple keys at a time.
  (merge! cfg {:val1 6 :val2 "neat"})
  (get-map cfg [:val1 :val2])

  ;; Perform read/writes in all sorts of transactions!
  (in-transaction
   cfg
   (fn [tx]
     (put! tx :green :emerald)
     (merge! tx {:blue :lapis :red :ruby})
     (println (get-map tx [:green :blue :red]))
     (println (get-value tx :green))))

  (in-exclusive-transaction
   cfg
   (fn [tx]
     (put! tx :green :g)
     (merge! tx {:blue :b :red :r})
     (println (get-map tx [:green :blue :red]))
     (println (get-value tx :green))))

  (try (in-readonly-transaction
        cfg
        (fn [tx]
          (println (get-value tx :green))
          (put! tx :explosion "boom!")))
       (catch Exception e
         (ex-message e))))
