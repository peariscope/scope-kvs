(ns scope-kvs.serializer
  (:import [java.nio ByteBuffer]
           [jetbrains.exodus
            ByteIterable ArrayByteIterable])
  (:require [clojure.data.fressian :as fress]))

(defn clj->entry
  "Serializes a Clojure datastructure into a ByteInterable via data.fressian/write."
  ^ByteIterable [value]
  (let [^ByteBuffer byte-buffer (fress/write value)
        ^bytes b-array          (byte-array (.remaining byte-buffer))]
    ;; There is a ByteBufferByteIterable class, but it does not fully implement the
    ;; ByteInterable interface (.getBytesUnsafe specifically). This breaks read/write operations.
    ;; Therefore, we unwrap the ByteBuffer into a byte[] and construct an ArrayByteInterable.
    (.get byte-buffer b-array)
    (ArrayByteIterable. b-array)))

(defn entry->clj
  "Convert a ByteIterable back into a Clojure datastructure via data.fressian/read."
  [^ByteIterable byte-iterable]
  (when byte-iterable
    (->> (.getBytesUnsafe byte-iterable)
         ByteBuffer/wrap
         fress/read)))
